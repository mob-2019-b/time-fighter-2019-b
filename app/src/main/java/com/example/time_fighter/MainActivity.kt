package com.example.time_fighter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var welcomeMessageTextView: TextView
    private lateinit var welcomeSubtitleTextView: TextView
    private lateinit var changeTextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        welcomeMessageTextView = findViewById(R.id.welcome_message)
        welcomeSubtitleTextView = findViewById(R.id.welcome_subtitle)
        changeTextButton = findViewById(R.id.change_text_button)

        changeTextButton.setOnClickListener { changeMessageAndSubtitle() }
    }


    private fun changeMessageAndSubtitle(){
        welcomeMessageTextView.text = getString(R.string.new_welcome_message)
        welcomeSubtitleTextView.text = getString(R.string.new_welcome_subtitle)
    }
}
